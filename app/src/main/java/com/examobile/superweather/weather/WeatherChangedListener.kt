package com.examobile.superweather.weather

import com.examobile.superweather.core.model.Weather

/**
 * Created by malgo on 29.12.2017.
 */
interface WeatherChangedListener{
    fun onWeatherChanged(weather: Weather)
    fun onCompleted()
    fun onError(e: Throwable?)
    fun onStarted()
}