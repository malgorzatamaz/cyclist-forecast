package com.examobile.superweather.weather

import com.examobile.superweather.network.NetworkService

interface WeatherService{
    fun setListener(weatherChangedListener : WeatherChangedListener?)
    fun getNorwayWeather(lat: Double, lng: Double)
    fun getYahooService(): NetworkService
    fun getNorwayService(): NetworkService
    fun getYahooResponse(lat: Double, lng: Double)
    fun getNorwaySunriseSunset(lat: Double, lng: Double)
}