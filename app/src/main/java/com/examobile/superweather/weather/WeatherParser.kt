package com.examobile.superweather.weather

import com.examobile.superweather.core.model.Weather
import com.examobile.superweather.core.utils.NorwaySunXmlParser
import com.examobile.superweather.core.utils.NorwayWeatherXmlParser
import com.examobile.superweather.core.utils.YahooXmlParser
import org.xml.sax.InputSource
import java.io.InputStream
import javax.xml.parsers.SAXParserFactory

/**
 * Created by malgo on 29.12.2017.
 */
class WeatherParser {
    companion object {
        fun parseNorwaySunXML(input: InputStream): Weather {
            val weather = Weather()

            val xmlParser = NorwaySunXmlParser()

            // create a XMLReader from SAXParser
            val xmlReader = SAXParserFactory.newInstance().newSAXParser()
                    .xmlReader
            // create a SAXXMLHandler
            // store handler in XMLReader
            xmlReader.contentHandler = xmlParser
            // the process starts
            try {
                xmlReader.parse(InputSource(input))
            }
            catch (e: Exception){
                e.printStackTrace()
            }

//            weather.sunrise = xmlParser.sunrise
//            weather.sunset = xmlParser.sunset

            return weather
        }

            fun parseNorwayWeatherXML(input: InputStream): Weather {
            val weather = Weather()
            val xmlParser = NorwayWeatherXmlParser()

            // create a XMLReader from SAXParser
            val xmlReader = SAXParserFactory.newInstance().newSAXParser()
                    .xmlReader
            // create a SAXXMLHandler
            // store handler in XMLReader
            xmlReader.contentHandler = xmlParser
            // the process starts
            try {
                xmlReader.parse(InputSource(input))
            }
            catch (e: Exception){
                e.printStackTrace()
            }

            weather.temperature = xmlParser.temperature
            weather.humidity = xmlParser.humidity
            weather.pressure = xmlParser.pressure
            weather.windSpeed = xmlParser.windSpeed
            weather.windDirection = xmlParser.windDirection
            weather.cloudiness = xmlParser.cloudiness

            return weather
        }


        fun parseYahooXml(input: InputStream): Weather {
            val weather = Weather()
            val xmlParser = YahooXmlParser()

            // create a XMLReader from SAXParser
            val xmlReader = SAXParserFactory.newInstance().newSAXParser()
                    .xmlReader
            // create a SAXXMLHandler
            // store handler in XMLReader
            xmlReader.contentHandler = xmlParser
            // the process starts
            try {
                xmlReader.parse(InputSource(input))
            }
            catch (e: Exception){
                e.printStackTrace()
            }

//            weather.sunset = xmlParser.sunset
//            weather.sunrise = xmlParser.sunrise
            weather.pressure = xmlParser.pressure

            return weather
        }
    }
}