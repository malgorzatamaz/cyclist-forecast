package com.examobile.superweather.weather

import com.examobile.superweather.core.model.Forecast
import com.examobile.superweather.core.utils.Formatter
import com.examobile.superweather.network.NetworkService
import com.examobile.superweather.network.RetrofitClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subscribers.DefaultSubscriber;
import junit.framework.Test
import okhttp3.ResponseBody
import java.util.logging.Logger
import io.reactivex.observers.DisposableObserver
import java.util.*

/**
 * Created by malgo on 21.12.2017.
 */
class WeatherServiceImpl : WeatherService {
    private var yahooService: NetworkService? = null
    private var norwayService: NetworkService? = null
    private val norwayForecastQuery = "https://api.met.no/weatherapi/"
    private val yahooForecastQuery = "https://query.yahooapis.com"//"http://query.yahooapis.com/v1/public/yql?q=select * from weather.forecast where woeid in (SELECT woeid FROM geo.places WHERE text=%22("

    private var weatherChangedListener: WeatherChangedListener? = null
    //val norwayForecastQuery = "http://api.met.no/weatherapi/locationforecast/1.9/?lat={0};lon={1}"

    private val norwayResponseSubscriber: DisposableObserver<ResponseBody> = object : DisposableObserver<ResponseBody>() {
        override fun onError(e: Throwable) {
            if (weatherChangedListener != null)
                weatherChangedListener!!.onError(e)

            Logger.getLogger(Test::class.java.name).warning(e.toString())
        }

        override fun onNext(response: ResponseBody) {
            if (response == null)
                return

            val weather = WeatherParser.parseNorwayWeatherXML(response.byteStream())

            if (weatherChangedListener != null) {
                weatherChangedListener!!.onWeatherChanged(weather)
            }
        }

        override fun onComplete() {
            if (weatherChangedListener != null)
                weatherChangedListener!!.onCompleted()


            Logger.getLogger(Test::class.java.name).warning("Completed")
        }
    }


    private val norwaySunResponseSubscriber: DisposableObserver<ResponseBody> = object : DisposableObserver<ResponseBody>() {
        override fun onError(e: Throwable) {
            if (weatherChangedListener != null)
                weatherChangedListener!!.onError(e)

            Logger.getLogger(Test::class.java.name).warning(e.toString())
        }

        override fun onNext(response: ResponseBody) {
            if (response == null)
                return

            val weather = WeatherParser.parseNorwaySunXML(response.byteStream())

            if (weatherChangedListener != null) {
                weatherChangedListener!!.onWeatherChanged(weather)
            }
        }

        override fun onComplete() {
            if (weatherChangedListener != null)
                weatherChangedListener!!.onCompleted()


            Logger.getLogger(Test::class.java.name).warning("Completed")
        }
    }

    private val yahooResponseSubscriber: DisposableObserver<ResponseBody> = object : DisposableObserver<ResponseBody>() {
        override fun onError(e: Throwable) {
            if (weatherChangedListener != null)
                weatherChangedListener!!.onError(e)

            Logger.getLogger(Test::class.java.name).warning(e.toString())
        }

        override fun onNext(response: ResponseBody) {
            if (response == null)
                return

            val weather = WeatherParser.parseYahooXml(response.byteStream())

            if (weatherChangedListener != null) {
                weatherChangedListener!!.onWeatherChanged(weather)
            }
        }

        override fun onComplete() {
            if (weatherChangedListener != null)
                weatherChangedListener!!.onCompleted()
        }
    }

    private val weatherSubscriber: DefaultSubscriber<Forecast> = object : DefaultSubscriber<Forecast>() {
        override fun onComplete() {
            if (weatherChangedListener != null)
                weatherChangedListener!!.onCompleted()
        }

        override fun onError(e: Throwable?) {
            if (weatherChangedListener != null)
                weatherChangedListener!!.onError(e)

            Logger.getLogger(Test::class.java.name).warning(e.toString())
        }

        override fun onNext(forecast: Forecast?) {
//            if (weatherChangedListener != null && weather != null) {
//                weatherChangedListener!!.onWeatherChanged(weather)
//            }
        }
    }


    override fun getYahooService(): NetworkService {
        return RetrofitClient.getClient(yahooForecastQuery)!!.create<NetworkService>(NetworkService::class.java)
    }

    override fun getNorwayService(): NetworkService {
        return RetrofitClient.getClient(norwayForecastQuery)!!.create<NetworkService>(NetworkService::class.java)
    }

    override fun setListener(weatherChangedListener: WeatherChangedListener?) {
        this.weatherChangedListener = weatherChangedListener
    }

    constructor() {
       // yahooService = getYahooService()
        norwayService = getNorwayService()
    }

    override fun getNorwayWeather(lat: Double, lng: Double) {
        if (weatherChangedListener != null) {
            this.weatherChangedListener!!.onStarted()
        }

        norwayService?.getWeatherResponse(lat, lng)?.
                subscribeOn(Schedulers.newThread())?.
                observeOn(AndroidSchedulers.mainThread())?.
                subscribe(norwayResponseSubscriber)
    }

    override fun getNorwaySunriseSunset(lat: Double, lng: Double) {
        if (weatherChangedListener != null) {
            this.weatherChangedListener!!.onStarted()
        }


        var time =  Formatter.toSimpleString(Date())

        norwayService?.getSunriseSunset(lat, lng, time)?.
                subscribeOn(Schedulers.newThread())?.
                observeOn(AndroidSchedulers.mainThread())?.
                subscribe(norwaySunResponseSubscriber)
    }

    override fun getYahooResponse(lat: Double, lng: Double) {
        if (weatherChangedListener != null) {
            this.weatherChangedListener!!.onStarted()
        }

        yahooService?.getWeatherResponse(lat, lng)?.
                subscribeOn(Schedulers.newThread())?.
                observeOn(AndroidSchedulers.mainThread())?.
                subscribeWith(yahooResponseSubscriber)
    }
}