package com.examobile.superweather.weather

import com.examobile.superweather.R
import com.examobile.superweather.core.enums.WeatherState

/**
 * Created by malgo on 29.12.2017.
 */
public class WeatherHelper {
    companion object {
        fun stateToImage(weatherState: WeatherState): Int {
            when(weatherState){
                WeatherState.Cloudy -> return R.drawable.cloudy
                WeatherState.Snowy -> return R.drawable.snowy
                WeatherState.Rainy -> return R.drawable.rainy
                WeatherState.Stormy -> return R.drawable.stormy
                WeatherState.Sunny -> return R.drawable.sun
                WeatherState.Windy -> return R.drawable.windy
            }

            return R.drawable.cloudy
        }

        fun codeToState(code: Int): WeatherState {
            when(code){
                0-> return WeatherState.Cloudy
            }

            return WeatherState.Default
        }
    }
}