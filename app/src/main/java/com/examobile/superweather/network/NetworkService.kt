package com.examobile.superweather.network

import com.examobile.superweather.core.model.Forecast
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface NetworkService {
    @GET("locationforecast/1.9")
    fun getWeatherResponse(@Query("lat") lat: Double, @Query("lon") lng: Double): Observable<ResponseBody>

    @GET("locationforecast/1.9")
    fun getWeather(@Query("lat") lat: Double, @Query("lon") lng: Double): Observable<Forecast>

    @GET("v1/public/yql")
    fun getWeather(@Query("q") query: String): Call<String>

    @GET("sunrise/1.1")
    fun getSunriseSunset(@Query("lat")lat: Double, @Query("lon")lng: Double,@Query("date") time: String): Observable<ResponseBody>
}