package com.examobile.superweather.dagger

import com.examobile.superweather.presenters.MainPresenter
import com.examobile.superweather.presenters.MainPresenterImpl
import com.examobile.superweather.presenters.MainView
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PresenterModule {
    @Provides
    @Singleton
    fun provideMainPresenter(): MainPresenter = MainPresenterImpl()
}