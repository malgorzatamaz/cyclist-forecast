package com.examobile.superweather.dagger

import com.examobile.superweather.weather.WeatherService
import com.examobile.superweather.weather.WeatherServiceImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class WeatherServiceModule {
    @Provides
    @Singleton
    fun provideServiceModule(): WeatherService = WeatherServiceImpl()
}