package com.examobile.superweather.dagger

import android.app.Activity
import com.examobile.superweather.activities.MainActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, PresenterModule::class,WeatherServiceModule::class])
interface AppComponent{
    fun inject(target: MainActivity)
}