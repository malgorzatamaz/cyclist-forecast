package com.examobile.superweather.location

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat.checkSelfPermission
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.OnSuccessListener

class LocationManager {
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private val PERMISSION_ACCESS_COURSE_LOCATION = 9999
    private var locationListener: LocationListener? = null

    private var activity: Activity

    constructor(activity: Activity) {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(activity)
        this.activity = activity
    }

    fun setLocationListener(locationListener: LocationListener) {
        this.locationListener = locationListener
    }

    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == PERMISSION_ACCESS_COURSE_LOCATION) {
            if (permissions.isNotEmpty()
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                getLocation()
        } else {

        }
    }

    fun getLocation() {
        if (checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mFusedLocationClient!!.lastLocation.addOnSuccessListener(activity) { location ->
                if (locationListener != null) {
                    locationListener!!.onLocationFound(location)
                }
            }
        } else {
            ActivityCompat.requestPermissions(activity, Array(1, { Manifest.permission.ACCESS_COARSE_LOCATION }),
                    PERMISSION_ACCESS_COURSE_LOCATION)
        }
    }
}
