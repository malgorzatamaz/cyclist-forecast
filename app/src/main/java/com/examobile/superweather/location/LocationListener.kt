package com.examobile.superweather.location

import android.location.Location

/**
 * Created by malgo on 29.12.2017.
 */

interface LocationListener{
    fun onLocationFound(location: Location)
}