package com.examobile.superweather.core.model

import com.examobile.superweather.core.enums.WeatherState
import com.examobile.superweather.core.enums.WindDirection

class Weather {
    val noValue = -11111111.0

    var temperature: Double = noValue
    var humidity: Double = noValue
    var pressure: Double = noValue
    var windSpeed: Double = noValue
    var cloudiness: Double = noValue
    var sunrise: Long = -1
    var sunset: Long = -1
    var state = WeatherState.Default
    var windDirection: WindDirection = WindDirection.N
}
