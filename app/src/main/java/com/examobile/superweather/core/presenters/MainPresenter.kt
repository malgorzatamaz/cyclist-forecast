package com.examobile.superweather.presenters

import com.examobile.superweather.weather.WeatherService
import java.lang.ref.WeakReference

/**
 * Created by malgo on 21.12.2017.
 */
interface MainPresenter {
    fun setService(weatherService: WeatherService)
    fun setMainView(mainView: MainView)
    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray)
}