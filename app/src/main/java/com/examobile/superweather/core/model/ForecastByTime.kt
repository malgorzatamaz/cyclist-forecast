package com.examobile.superweather.core.model

import java.util.*

class ForecastByTime {
    var stringFrom = ""
    var stringTo = ""

    lateinit var timeFrom: Date
    lateinit var timeTo: Date

    lateinit var weather: Weather
}