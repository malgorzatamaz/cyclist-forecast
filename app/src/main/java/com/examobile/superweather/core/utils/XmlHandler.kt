package com.examobile.superweather.core.utils

import org.xml.sax.*


open class XmlHandler : SAXException(), ContentHandler, ErrorHandler {
    protected val characters = StringBuilder()

    private lateinit var locator: Locator

    val data: String
        get() = characters.toString()

    @Throws(SAXException::class)
    override fun warning(exception: SAXParseException) {
        System.err.println(exception)
    }

    @Throws(SAXException::class)
    override fun error(exception: SAXParseException) {
        throw SAXException(exception)
    }

    @Throws(SAXException::class)
    override fun fatalError(exception: SAXParseException) {
        throw SAXException(exception)
    }

    override fun setDocumentLocator(locator: Locator) {
        this.locator = locator
    }

    @Throws(SAXException::class)
    override fun startDocument() {
        characters.setLength(0)
    }

    @Throws(SAXException::class)
    override fun endDocument() {
    }

    @Throws(SAXException::class)
    override fun startPrefixMapping(prefix: String, uri: String) {
    }

    @Throws(SAXException::class)
    override fun endPrefixMapping(prefix: String) {
    }

    @Throws(SAXException::class)
    override fun startElement(uri: String, localName: String, qName: String,
                              atts: Attributes) {
        characters.setLength(0)
    }

    @Throws(SAXException::class)
    override fun endElement(uri: String, localName: String, qName: String) {
        characters.setLength(0)
    }

    @Throws(SAXException::class)
    override fun characters(ch: CharArray, start: Int, length: Int) {
        characters.append(ch, start, length)
    }

    @Throws(SAXException::class)
    override fun ignorableWhitespace(ch: CharArray, start: Int, length: Int) {
    }

    @Throws(SAXException::class)
    override fun processingInstruction(target: String, data: String) {
    }

    @Throws(SAXException::class)
    override fun skippedEntity(name: String) {
    }
}