package com.examobile.superweather.activities

import android.app.Activity
import android.app.PendingIntent.getActivity
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.examobile.superweather.R
import com.examobile.superweather.application.WeatherApplication
import com.examobile.superweather.core.model.Weather
import com.examobile.superweather.presenters.MainPresenter
import com.examobile.superweather.presenters.MainView
import com.examobile.superweather.weather.WeatherChangedListener
import com.examobile.superweather.weather.WeatherService
import javax.inject.Inject

class MainActivity : AppCompatActivity(), MainView, WeatherChangedListener {
    override fun onCompleted() {
    }

    override fun onError(e: Throwable?) {
    }

    override fun onStarted() {
    }

    override fun onWeatherChanged(weather: Weather) {

    }

    override fun getCurrentActivity(): Activity {
        return this
    }

    override fun getContext(): Context {
        return this
    }

    @Inject lateinit var presenter: MainPresenter
    @Inject lateinit var weatherService: WeatherService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        (application as WeatherApplication).appComponent.inject(this)
        weatherService.setListener(this)
        presenter.setService(weatherService)
        presenter.setMainView(this)

        //TODO:
        //1. Use data binding util
        //2. Dependency Injection with Dagger2
        //3. RxJava observable
        //4. Unit test with Robolectric
        //5. UI test with Espresso
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        presenter.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onResume() {
        super.onResume()
    }
}
