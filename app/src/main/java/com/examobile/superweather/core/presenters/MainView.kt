package com.examobile.superweather.presenters

import android.app.Activity
import android.app.Application
import android.content.Context
import com.examobile.superweather.core.model.Weather

/**
 * Created by malgo on 21.12.2017.
 */

interface MainView{
    fun getContext() : Context
    fun getCurrentActivity() : Activity
    fun onWeatherChanged(weather: Weather)
}