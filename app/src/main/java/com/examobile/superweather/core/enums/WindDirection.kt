package com.examobile.superweather.core.enums

public enum class WindDirection{
    N,
    NW,
    NE,
    SW,
    SE,
    S
}