package com.examobile.superweather.presenters

import android.location.Location
import com.examobile.superweather.core.model.Weather
import com.examobile.superweather.location.LocationListener
import com.examobile.superweather.location.LocationManager
import com.examobile.superweather.weather.WeatherChangedListener
import com.examobile.superweather.weather.WeatherService

class MainPresenterImpl : MainPresenter {
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        locationManager?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private lateinit var service: WeatherService
    private lateinit var mainView: MainView
    private var locationManager: LocationManager? = null
    var weatherChangedListener = object :  WeatherChangedListener{
        override fun onCompleted() {
        }

        override fun onError(e: Throwable?) {
        }

        override fun onStarted() {
        }

        override fun onWeatherChanged(weather: Weather) {
           if(mainView != null)
               mainView.onWeatherChanged(weather)
        }
    }


    private var locationListener = object : LocationListener {
        override fun onLocationFound(location: Location) {
            service.getNorwayWeather(location.latitude, location.longitude)
        }
    }

    override fun setService(weatherService: WeatherService) {
        this.service = weatherService
    }

    override fun setMainView(mainView: MainView) {
        this.mainView = mainView


        locationManager = LocationManager(mainView.getCurrentActivity())
        locationManager!!.setLocationListener(locationListener)
        locationManager!!.getLocation()
    }
}