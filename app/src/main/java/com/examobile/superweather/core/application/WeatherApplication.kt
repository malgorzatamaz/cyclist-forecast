package com.examobile.superweather.application

import android.app.Application
import com.examobile.superweather.dagger.AppComponent
import com.examobile.superweather.dagger.AppModule
import com.examobile.superweather.dagger.DaggerAppComponent

/**
 * Created by malgo on 20.12.2017.
 */

class WeatherApplication: Application() {
    lateinit var appComponent: AppComponent

    private fun initDagger(app: WeatherApplication): AppComponent =
            DaggerAppComponent.builder()
                    .appModule(AppModule(app))
                    .build()

    override fun onCreate() {
        super.onCreate()
        appComponent = initDagger(this)
    }
}