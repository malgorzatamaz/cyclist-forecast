package com.examobile.superweather.core.viewmodel;

import com.examobile.superweather.core.enums.WeatherState;
import com.examobile.superweather.core.enums.WindDirection

public class WeatherViewModel {
    var temperature: String = ""
    var humidity: String = ""
    var pressure:  String = ""
    var windSpeed:  String = ""
    var cloudiness:  String = ""
    var sunrise: String = ""
    var sunset: String = ""
    var state = WeatherState.Default;
    var windDirection: WindDirection = WindDirection.N;
}
