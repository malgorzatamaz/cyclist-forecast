package com.examobile.superweather.core.enums

/**
 * Created by malgo on 28.12.2017.
 */
public enum class WeatherState{
    Windy,
    Snowy,
    Sunny,
    Cloudy,
    Rainy,
    Stormy,
    Default
}