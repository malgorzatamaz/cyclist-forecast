package com.examobile.superweather.core.utils

import android.util.Log
import org.xml.sax.Attributes
import org.xml.sax.SAXException

class YahooXmlParser : XmlHandler() {

    var pressure: Double = 0.0
    var sunrise: String = ""
    var sunset: String = ""

    /*--------------------------------------------------------------------------*/
    /*							START ELEMENT									*/
    /*--------------------------------------------------------------------------*/


    @Throws(SAXException::class)
    override fun startElement(namespaceURI: String,
                              localName: String,
                              qName: String,
                              attr: Attributes) {

        super.startElement(namespaceURI, localName, qName, attr)

        if (qName.equals("time", ignoreCase = true)) {
            for (i in 0 until attr.length) {
                if (attr.getLocalName(i) == "from") {
                    sunrise = attr.getValue(i)
                }
            }
        }

        if (qName.equals("time", ignoreCase = true)) {
            for (i in 0 until attr.length) {
                if (attr.getLocalName(i) == "to") {
                    sunset = attr.getValue(i)
                }
            }
        }

        if (qName.equals("pressure", ignoreCase = true)) {
            Log.d("Barometer", "Got pressure")

            for (i in 0 until attr.length) {
                if (attr.getLocalName(i) == "value") {
                    pressure = attr.getValue(i).toDouble()
                }
            }
        }
    }

    /*--------------------------------------------------------------------------*/
    /*							END ELEMENT										*/
    /*--------------------------------------------------------------------------*/

    @Throws(SAXException::class)
    override fun endElement(namespaceURI: String,
                            localName: String,
                            qName: String) {
        super.endElement(namespaceURI, localName, qName)

        Log.d("Barometer", "END: $localName")

//        if (localName == "time") {
//            throw SAXException()
//        }
    }


    @Throws(SAXException::class)
    override fun characters(ch: CharArray,
                            start: Int,
                            length: Int) {

        super.characters(ch, start, length)
    }

}
