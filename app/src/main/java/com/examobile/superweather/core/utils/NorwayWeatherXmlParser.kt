package com.examobile.superweather.core.utils

import android.util.Log
import com.examobile.superweather.core.enums.WindDirection
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

/**
 * Created by examobilesa on 28.08.2017.
 */

class NorwayWeatherXmlParser : XmlHandler() {
    var timeFrom: String = "";
    var timeTo:  String = "";

    var temperature: Double = 0.0
    var humidity: Double = 0.0
    var windSpeed: Double = 0.0
    var windDirection: WindDirection = WindDirection.N

    var pressure: Double = 0.0
    var cloudiness: Double = 0.0

    /*--------------------------------------------------------------------------*/
    /*							START ELEMENT									*/
    /*--------------------------------------------------------------------------*/


    @Throws(SAXException::class)
    override fun startElement(namespaceURI: String,
                              localName: String,
                              qName: String,
                              attr: Attributes) {

        super.startElement(namespaceURI, localName, qName, attr)

        if (qName.equals("time", ignoreCase = true)) {
            for (i in 0 until attr.length) {
                if (attr.getLocalName(i) == "from") {
                    timeFrom = attr.getValue(i)
                }
            }
        }

        if (qName.equals("time", ignoreCase = true)) {
            for (i in 0 until attr.length) {
                if (attr.getLocalName(i) == "to") {
                    timeTo = attr.getValue(i)
                }
            }
        }

        if (qName.equals("temperature", ignoreCase = true)) {
            for (i in 0 until attr.length) {
                if (attr.getLocalName(i) == "value") {
                    temperature = attr.getValue(i).toDouble()
                }
            }
        }
        if (qName.equals("windSpeed", ignoreCase = true)) {
            for (i in 0 until attr.length) {
                if (attr.getLocalName(i) == "mps") {
                    windSpeed = attr.getValue(i).toDouble()
                }
            }
        }
        if (qName.equals("windDirection", ignoreCase = true)) {
            for (i in 0 until attr.length) {
                if (attr.getLocalName(i) == "deg") {

                    //TODO preapare
                    //windDirection = attr.getValue(i).toDouble()
                }
            }
        }
        if (qName.equals("cloudiness", ignoreCase = true)) {
            for (i in 0 until attr.length) {
                if (attr.getLocalName(i) == "percent") {
                    cloudiness = attr.getValue(i).toDouble()
                }
            }
        }
        if (qName.equals("humidity", ignoreCase = true)) {
            for (i in 0 until attr.length) {
                if (attr.getLocalName(i) == "value") {
                    humidity = attr.getValue(i).toDouble()
                }
            }
        }
        if (qName.equals("pressure", ignoreCase = true)) {

            for (i in 0 until attr.length) {
                if (attr.getLocalName(i) == "value") {
                    pressure = attr.getValue(i).toDouble()

                }
            }
        }
    }

    /*--------------------------------------------------------------------------*/
    /*							END ELEMENT										*/
    /*--------------------------------------------------------------------------*/

    @Throws(SAXException::class)
    override fun endElement(namespaceURI: String,
                            localName: String,
                            qName: String) {
        super.endElement(namespaceURI, localName, qName)

//        Log.d("Barometer", "END: $localName")

//        if (localName == "time") {
//            throw SAXException()
//        }
    }


    @Throws(SAXException::class)
    override fun characters(ch: CharArray,
                            start: Int,
                            length: Int) {

        super.characters(ch, start, length)
    }

}