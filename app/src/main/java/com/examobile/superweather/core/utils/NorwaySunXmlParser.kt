package com.examobile.superweather.core.utils

import org.xml.sax.Attributes
import org.xml.sax.SAXException
import java.text.ParseException
import java.text.SimpleDateFormat

class NorwaySunXmlParser: XmlHandler(){
    var sunrise: String = "";
    var sunset:  String = "";
    val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")


    fun getSurise(): Long{
        var sunriseValue: Long = -1

        if(sunrise != "" && sunrise.contains("T") && sunrise.contains("Z")){
            try {
                val date = format.parse(sunrise)
                sunriseValue = date.time
            } catch (e: ParseException) {
                e.printStackTrace()
            }

        }

        return sunriseValue
    }

    fun getSunset(): Long{
        var sunsetValue: Long = -1

        if(sunset != "" && sunset.contains("T") && sunset.contains("Z")){
            try {
                val date = format.parse(sunset)
                sunsetValue = date.time
            } catch (e: ParseException) {
                e.printStackTrace()
            }

        }

        return sunsetValue
    }

    /*--------------------------------------------------------------------------*/
    /*							START ELEMENT									*/
    /*--------------------------------------------------------------------------*/


    @Throws(SAXException::class)
    override fun startElement(namespaceURI: String,
                              localName: String,
                              qName: String,
                              attr: Attributes) {

        super.startElement(namespaceURI, localName, qName, attr)

        if (qName.equals("sun", ignoreCase = true)) {
            for (i in 0 until attr.length) {
                if (attr.getLocalName(i) == "rise") {
                    sunrise = attr.getValue(i)
                }

                if (attr.getLocalName(i) == "set") {
                    sunset = attr.getValue(i)
                }
            }
        }

    }

    /*--------------------------------------------------------------------------*/
    /*							END ELEMENT										*/
    /*--------------------------------------------------------------------------*/

    @Throws(SAXException::class)
    override fun endElement(namespaceURI: String,
                            localName: String,
                            qName: String) {
        super.endElement(namespaceURI, localName, qName)

//        Log.d("Barometer", "END: $localName")

//        if (localName == "time") {
//            throw SAXException()
//        }
    }


    @Throws(SAXException::class)
    override fun characters(ch: CharArray,
                            start: Int,
                            length: Int) {

        super.characters(ch, start, length)
    }

}