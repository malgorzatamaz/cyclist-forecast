package com.examobile.superweather.core.utils

import java.text.SimpleDateFormat
import java.util.*

class Formatter{
    companion object {
        @JvmStatic
        fun toSimpleString(date: Date): String {
            val format = SimpleDateFormat("yyyy-MM-dd")
            return format.format(date)
        }
    }
}