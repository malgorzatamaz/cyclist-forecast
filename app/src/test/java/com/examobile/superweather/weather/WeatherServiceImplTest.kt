package com.examobile.superweather.weather

import android.util.Log
import com.examobile.superweather.core.model.Weather
import io.reactivex.android.plugins.RxAndroidPlugins
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before

import java.util.concurrent.CountDownLatch

import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers


class WeatherServiceImplTest {
    private lateinit var service: WeatherServiceImpl



    @Before
    fun init() {
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setComputationSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setNewThreadSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }

        service = WeatherServiceImpl()
    }

    private lateinit var doneSignal: CountDownLatch

    @Test
    fun getNorwayWeather() {
        service.setListener(object : WeatherChangedListener {
            override fun onWeatherChanged(weather: Weather) {
                assert(weather.temperature != weather.noValue)
                doneSignal.countDown()
            }

            override fun onCompleted() {
            }

            override fun onError(e: Throwable?) {
                doneSignal = CountDownLatch(1)
            }

            override fun onStarted() {
            }
        })

        doneSignal = CountDownLatch(1)
        service.getNorwayWeather(49.1,19.2)
        doneSignal.await()
    }

    @Test
    fun getNorwaySunset() {
        service.setListener(object : WeatherChangedListener {
            override fun onWeatherChanged(weather: Weather) {
                assert(weather.sunrise != "")

                doneSignal.countDown()
            }

            override fun onCompleted() {
            }

            override fun onError(e: Throwable?) {
                doneSignal = CountDownLatch(1)
            }

            override fun onStarted() {
            }
        })

        doneSignal = CountDownLatch(1)
        service.getNorwaySunriseSunset(49.1,19.2)
        doneSignal.await()
    }

    @Test
    fun getYahooResponse() {
    }
}